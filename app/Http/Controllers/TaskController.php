<?php

namespace App\Http\Controllers;

use App\Http\Requests\TaskRequest;
use App\Services\TaskService;
use http\Env\Response;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function __construct(protected TaskService $taskService)
    {
    }

    public function index()
    {
        $list = $this->taskService->getAll();
        return view('home', compact('list'));
    }

    public function finish($id)
    {
        $this->taskService->finish($id);

        return response()->json(['status' => true]);
    }

    public function notFinish($id)
    {
        $this->taskService->notFinish($id);

        return response()->json(['status' => true]);
    }

    public function addTask(Request $request)
    {
        $data = [
            'name' => $request->name,
            'due_date' => $request->date
        ];

        $addTask = $this->taskService->addTask($data);

        if ($addTask) {
            $itemTaskView = view('task-item', ['item' => $addTask])->render();

            return \response()->json(['status' => true, 'item' => $itemTaskView]);
        }
    }

    public function editTask($id)
    {
        $findTask = $this->taskService->findTask($id);

        return \response()->json(['status' => true, 'task' => $findTask]);
    }

    public function updateTask(TaskRequest $request)
    {
        $data = [
            'name' => $request->name,
            'due_date' => $request->date
        ];

        $id = $request->id;

        $this->taskService->update($id, $data);

        return redirect()->back()->with('message_success', 'Update task success');
    }

    public function deleteTask($id)
    {
        $ids[0] = $id;

        $this->taskService->deleteTask($ids);

        return \response()->json(['status' => true]);
    }

    public function deleteMultiTask(Request $request)
    {
        $arrId = $request->ids;

        $this->taskService->deleteMultiTask($arrId);

        return \response()->json(['status' => true]);
    }
}
