<?php

namespace App\Repository;

abstract class BaseRepository
{
    protected $_model;

    public function __construct()
    {
        $this->setModel();
    }

    abstract public function getModel();

    public function setModel()
    {
        $this->_model = app()->make($this->getModel());
    }

    public function getAll()
    {
        return $this->_model->orderBy('id','desc')->get();
    }

    public function create($data)
    {
        return $this->_model->create($data);
    }

    public function update($id, $attribute = [])
    {
        return $this->_model->find($id)->update($attribute);
    }

    public function find($id)
    {
        return $this->_model->find($id);
    }

    public function delete($ids)
    {
        return $this->_model->whereIn('id',$ids)->delete();
    }
}

