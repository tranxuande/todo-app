<?php

namespace App\Repository;

use App\Models\Task;

class TaskRepository extends BaseRepository
{

    public function getModel()
    {
        return Task::class;
    }
}
