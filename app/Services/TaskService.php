<?php

namespace App\Services;

use App\Repository\TaskRepository;

class TaskService
{
    public function __construct(protected TaskRepository $taskRepository)
    {
    }

    public function getAll()
    {
        return $this->taskRepository->getAll();
    }

    public function finish($id)
    {
        $attribute['finish'] = '1';

        return $this->taskRepository->update($id, $attribute);
    }

    public function notFinish($id)
    {
        $attribute['finish'] = '0';

        return $this->taskRepository->update($id, $attribute);
    }

    public function addTask($data)
    {
        return $this->taskRepository->create($data);
    }

    public function findTask($id)
    {
        return $this->taskRepository->find($id);
    }

    public function update($id, $data)
    {
        return $this->taskRepository->update($id, $data);
    }

    public function deleteTask($id)
    {
        return $this->taskRepository->delete($id);
    }

    public function deleteMultiTask($arrId)
    {
        return $this->taskRepository->delete($arrId);
    }
}
