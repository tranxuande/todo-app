var setdate = document.querySelector(".set_date");
var settime = document.querySelector(".set_time");

var date = new Date().toDateString();
setdate.innerHTML = date;

setInterval(function () {
    var time = new Date().toLocaleTimeString();
    settime.innerHTML = time;
}, 500);

$.ajaxSetup({
    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
});

var totalRecord = $('#total-task');

$(document).ready(function () {
    totalRecord.text($('li').length)
})

$(document).on('click', '.checkbox', function () {
    let id = $(this).closest('.task-item').attr('id');
    let button = $(this).closest('.task-item').find('button');
    let icon = $(this).closest('.task-item').find('.icon-edit');
    if (this.checked) {
        $.ajax({
            type: 'post',
            url: '/finish/' + id,
            success: function (result) {
                if (result.status) {
                    $('#strike' + id).toggleClass('strike_none');
                    button.prop('disabled', true);
                }
            },
            error: function () {
                console.log('err')
            }
        });
    } else {
        $.ajax({
            type: 'post',
            url: '/not-finish/' + id,
            success: function (result) {
                if (result.status) {
                    $('#strike' + id).toggleClass('strike_none');
                    button.prop('disabled', false);
                }
            },
            error: function () {
                console.log('err')
            }
        });
    }

})

$('.btn-add-task').click(function () {
    let name = $('input[name="name"]').val();
    let date = $('input[type="date"]').val();

    if (name == '') {
        $('input[name="name"]').addClass('is-invalid');
        $('input[name="name"]').focus();
    } else if (date == '') {
        $('input[type="date"]').addClass('is-invalid');
        $('input[name="name"]').removeClass('is-invalid');
    } else {
        $('input[name="name"]').removeClass('is-invalid');
        $('input[type="date"]').removeClass('is-invalid');

        $.ajax({
            type: 'post',
            url: '/add-task',
            data: {
                name: name,
                date: date
            },
            success: function (result) {
                $('.list-task').prepend(result.item);
                totalRecord.text($('li').length);
            },
            error: function () {
                console.log('err');
            }
        })
    }

})

$(document).on('click', '.btn-edit', function () {
    let id = $(this).closest('.task-item').attr('id');
    $('#form-edit-task').find("input[name='name']").removeClass('is-invalid');
    $('#form-edit-task').find("input[name='date']").removeClass('is-invalid');

    $.ajax({
        type: 'get',
        url: '/edit-task/' + id,
        success: function (result) {
            $('#form-edit-task').find("input[name='id']").val(result.task.id);
            $('#form-edit-task').find("input[name='name']").val(result.task.name);
            $('#form-edit-task').find("input[name='date']").val(result.task.due_date);
            $('#edit-task-modal').modal('show');
        },
        error: function () {
            console.log('err');
        }
    })
})

$('#form-edit-task').submit(function () {
    let check = true;
    let name = $(this).find("input[name='name']").val();
    let date = $(this).find("input[name='date']").val();

    if (name == '') {
        $(this).find("input[name='name']").addClass('is-invalid');
        $(this).find("input[name='name']").focus();
        check = false;
    } else if (date == '') {
        $(this).find("input[name='date']").addClass('is-invalid');
        $(this).find("input[name='name']").removeClass('is-invalid');
        check = false;
    }

    return check;
});

setTimeout(function () {
    $('.alert-success').remove()
}, 2000);

$(document).on('click', '.icon-delete', function () {
    let id = $(this).closest('.task-item').attr('id');
    let taskItem = $(this).closest('.task-item');

    Swal.fire({
        title: '<h5>Are you sure delete ?</h5>',
        icon: 'question',
        showCancelButton: true,
        confirmButtonText: 'OK',
        cancelButtonText: 'Cancel',
    }).then(function (result) {
        if (result.value) {
            $.ajax({
                type: 'post',
                url: 'delete-task/' + id,
                success: function (result) {
                    taskItem.remove();
                    totalRecord.text($('li').length);
                },
                error: function () {
                    console.log('err')
                }
            })
        }
    })
})

$('.btn-clear-all').click(function () {
    let arr = [];

    $('li').each(function () {
        arr.push($(this).attr('id'));
    })

    Swal.fire({
        title: '<h5>Are you sure delete all record?</h5>',
        icon: 'question',
        showCancelButton: true,
        confirmButtonText: 'OK',
        cancelButtonText: 'Cancel',
    }).then(function (result) {
        if (result.value) {
            $.ajax({
                type: 'post',
                url: '/delete-multi',
                data: {ids: arr},
                success: function (result) {
                    $('li').remove();
                    totalRecord.text(0);
                },
                error: function () {
                    console.log('err')
                }
            })
        }
    })
})



