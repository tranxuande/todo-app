<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
          integrity="sha512-SfTiTlX6kk+qitfevl/7LibUOeJWlt9rbyDn92a1DqWOw9vWG2MFoays0sgObmWazO5BQPiFucnnEAjpAB+/Sw=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.1/dist/sweetalert2.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('home.css')}}">
    <title>TODO APP</title>
</head>
<body>
<div class="flex justify-center items-center min-h-screen bg-[#cbd7e3]">
    <div class="h-auto bg-white rounded-lg p-3 the">
        <div class="mt-3 text-sm text-[#8ea6c8] flex justify-between items-center">
            <p class="set_date">Thursday 28 May</p>
            <p class="set_time">8:00 AM</p>
        </div>
        <p class="text-xl font-semibold mt-2 text-[#063c76]">To-do List</p>
        <div class="w-full mt-4 flex text-sm flex-col text-center justify-center ">
            <div class="input-group mb-3">
                <input type="text" name="name" class="form-control input-name" placeholder="Enter task"
                       aria-label="Enter task"
                       aria-describedby="basic-addon2">
                <div class='col-md-5'>
                    <input type='date' class="form-control input-date" placeholder="Due date"/>
                </div>
                <div>
                    <button class="btn btn-add-task" type="button" style="background-color: #008CBA; color: white">
                        Add Task
                    </button>
                </div>
            </div>
        </div>
        <div class="d-flex justify-content-between">
            <h4 class="text-[#CC0099]">Total task(<span id="total-task" class="font-semibold">10</span>)</h4>
            <div>
                <button type="button" class="btn btn-outline-danger btn-clear-all"
                        style="background-color:#FF2011; color: white">
                    Clear All
                </button>
            </div>
        </div>
        @if(session('message_success'))
            <span class="px-3"></span>
            <div class="alert alert-success" role="alert">
                {{session('message_success')}}
            </div>
        @endif

        <ul class="my-4 list-task">
            @foreach($list as $item)
                @if($item->finish==1)
                    <li class="mt-4 task-item" id="{{$item->id}}">
                        <div class="flex gap-2">
                            <div class="w-9/12 h-12 bg-[#e0ebff] rounded-[7px] flex justify-start items-center px-3">
                                <input type="checkbox" class="checkbox" checked>
                                <div>
                                    <strike id="strike{{$item->id}}"
                                            class="text-sm ml-4 text-[#5b7a9d] font-semibold ">{{$item->name}}</strike>
                                    <div class="text-sm ml-4 text-[#808080] font-semibold ">{{$item->due_date}}</div>
                                </div>
                            </div>
                            <span
                                class="w-1/4 h-12 bg-[#e0ebff] rounded-[7px] flex justify-center text-sm text-[#5b7a9d] font-semibold items-center ">
                                    <button disabled class="btn-edit"><i
                                            class="fa fa-pencil-square-o cursor-pointer icon icon-edit"
                                            aria-hidden="true"></i></button>
                                    <span class="px-3"></span>
                                <i class="fa fa-trash-o cursor-pointer icon icon-delete" aria-hidden="true"></i>
                            </span>
                        </div>
                    </li>
                @else
                    <li class="mt-4 task-item" id="{{$item->id}}">
                        <div class="flex gap-2">
                            <div class="w-9/12 h-12 bg-[#e0ebff] rounded-[7px] flex justify-start items-center px-3">
                                <input type="checkbox" class="checkbox">
                                <div>
                                    <strike id="strike{{$item->id}}"
                                            class="strike_none text-sm ml-4 text-[#5b7a9d] font-semibold ">{{$item->name}}</strike>
                                    <div class="text-sm ml-4 text-[#808080] font-semibold ">{{$item->due_date}}</div>
                                </div>
                            </div>
                            <span
                                class="w-1/4 h-12 bg-[#e0ebff] rounded-[7px] flex justify-center text-sm text-[#5b7a9d] font-semibold items-center ">
                        <button class="btn-edit"><i class="fa fa-pencil-square-o cursor-pointer icon icon-edit"
                                                    aria-hidden="true"></i></button>
                        <span class="px-3"></span>
                        <i class="fa fa-trash-o cursor-pointer icon icon-delete" aria-hidden="true"></i>
                    </span>
                        </div>
                    </li>
                @endif
            @endforeach
        </ul>
    </div>
</div>

<div class="modal fade" id="edit-task-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit task</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form-edit-task" action="{{route('update')}}" method="post">
                @csrf
                <div class="modal-body">
                    <input type="hidden" name="id">
                    <input type="text" name="name" class="form-control input-name"
                           aria-describedby="basic-addon2">
                    <span class="px-3"></span>
                    <input type='date' name="date" class="form-control input-date"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"
                            style="background-color: #333645; color: white">Close
                    </button>
                    <button type="submit" class="btn btn-primary" style="background-color: #008CBA; color: white">
                        Save changes
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
<script src="https://cdn.tailwindcss.com/3.0.12"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.3/jquery.min.js"
        integrity="sha512-STof4xm1wgkfm7heWqFJVn58Hm3EtS31XFaagaa8VMReCXAkQnJZ+jEy8PCC/iT18dFy95WcExNHFTqLyp72eQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.1/dist/sweetalert2.all.min.js"></script>
<script src="{{asset('home.js')}}"></script>
</html>
