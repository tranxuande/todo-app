<li class="mt-4 task-item" id="{{$item->id}}">
    <div class="flex gap-2">
        <div class="w-9/12 h-12 bg-[#e0ebff] rounded-[7px] flex justify-start items-center px-3">
            <input type="checkbox" class="checkbox">
            <div>
                <strike id="strike{{$item->id}}"
                        class="strike_none text-sm ml-4 text-[#5b7a9d] font-semibold ">{{$item->name}}
                </strike>
                <div class="text-sm ml-4 text-[#808080] font-semibold ">{{$item->due_date}}</div>
            </div>
        </div>
        <span class="w-1/4 h-12 bg-[#e0ebff] rounded-[7px] flex justify-center text-sm text-[#5b7a9d] font-semibold items-center ">
            <button class="btn-edit">
                <i class="fa fa-pencil-square-o cursor-pointer icon icon-edit" aria-hidden="true"></i>
            </button>
            <span class="px-3"></span>
            <i class="fa fa-trash-o cursor-pointer icon icon-delete" aria-hidden="true"></i>
        </span>
    </div>
</li>
