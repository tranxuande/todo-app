<?php

use App\Http\Controllers\TaskController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::controller(TaskController::class)->group(function () {
    Route::get('', 'index');
    Route::post('finish/{id}','finish');
    Route::post('not-finish/{id}','notFinish');
    Route::post('add-task','addTask');
    Route::get('edit-task/{id}','editTask');
    Route::post('update-task','updateTask')->name('update');
    Route::post('delete-task/{id}','deleteTask');
    Route::post('delete-multi','deleteMultiTask');
});
